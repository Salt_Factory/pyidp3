\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Pyidp3 features}{3}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Existing features from Pyidp}{3}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Added features in Pyidp3}{3}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Added QOL in Pyidp3:}{4}{section.1.3}% 
\contentsline {section}{\numberline {1.4}Fixed bugs:}{4}{section.1.4}% 
\contentsline {chapter}{\numberline {2}Basic Tutorial}{7}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Requirements}{7}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Tutorial}{7}{section.2.2}% 
\contentsline {chapter}{\numberline {3}Examples using the Pyidp3 API}{13}{chapter.3}% 
\contentsline {section}{\numberline {3.1}simple\_inference}{13}{section.3.1}% 
\contentsline {section}{\numberline {3.2}harder\_inference}{14}{section.3.2}% 
\contentsline {section}{\numberline {3.3}definition\_test}{15}{section.3.3}% 
\contentsline {section}{\numberline {3.4}constructed\_from}{16}{section.3.4}% 
\contentsline {section}{\numberline {3.5}initial\_group\_assign}{16}{section.3.5}% 
\contentsline {section}{\numberline {3.6}initial\_group\_assign\_plus}{18}{section.3.6}% 
\contentsline {section}{\numberline {3.7}further\_group\_assign}{20}{section.3.7}% 
\contentsline {section}{\numberline {3.8}sudoku}{23}{section.3.8}% 
\contentsline {section}{\numberline {3.9}masyu}{24}{section.3.9}% 
\contentsline {chapter}{\numberline {4}Porting of Pyidp to Pyidp3}{27}{chapter.4}% 
\contentsline {chapter}{\numberline {5}The Pyidp3 API reference}{33}{chapter.5}% 
\contentsline {section}{\numberline {5.1}The typedIDP submodule:}{33}{section.5.1}% 
\contentsline {section}{\numberline {5.2}The idpobjects submodule:}{39}{section.5.2}% 
\contentsline {section}{\numberline {5.3}The idp\_py\_syntax submodule:}{40}{section.5.3}% 
\contentsline {section}{\numberline {5.4}The idp\_parse\_out submodule:}{40}{section.5.4}% 
\contentsline {chapter}{Python Module Index}{41}{section*.77}% 
\contentsline {chapter}{Index}{43}{section*.78}% 
