.. _api:

The Pyidp3 API reference
========================

This page contains the API reference for Pyidp3. 
This is an entirely autogenerated page, based on the docstring that are inside the code.
Some things might be wrong, or might be outdated. If you find any of such things, let me know and I'll fix them (or make a pullrequest).

As the entire layout can be a bit awkward, I also autogenerated UML diagrams.
This is the package structure:


.. image:: images/packages.png

And this is the entire class structure, with only the classnames:

.. image:: images/classes.png

For an overview of the entire class structure with methods and attributes, check the bottom of this page.

.. automodule:: pyidp3
    :members:

The typedIDP submodule:
-----------------------

This is the main, toplevel submodule and contains everything a normal user would need.

.. automodule:: pyidp3.typedIDP
    :members:

The idpobjects submodule:
-------------------------

This submodule contains an object for every kind of idp object.

.. automodule:: pyidp3.idpobjects
    :members:

The idp_py_syntax submodule:
----------------------------

This submodule contains everything needed to convert Pythonic data to IDP.
This is something that was support by the original Pyidp, but is no longer supported by Pyidp3.
The code might still work, it's just not being worked on.

.. automodule:: pyidp3.idp_py_syntax
    :members:

The idp_parse_out submodule:
----------------------------

This submodule contains all the code necessary to read IDP output and convert it to Python.

.. automodule:: pyidp3.idp_parse_out
    :members:


.. image:: images/classes_full.png
