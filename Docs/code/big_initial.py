#!/usr/bin/python3

"""
Testfile that checks whether ISSUE-1 (the {x..y} in structs) is still valid.
"""
from pathlib import Path
from pyidp3.typedIDP import *


home = str(Path.home())
idp = IDP(home+"/idp/usr/local/bin/idp")
idp.mxtimeout = 10
idp.nbmodels = 5

WoonDict = {1: 87, 2: 98, 3: 80, 4: 80, 5: 83, 6: 88, 7: 90, 8: 86, 9: 80, 10:
            53, 11: 98, 12: 57, 13: 86, 14: 82, 15: 98, 16: 83, 17: 15, 18: 88,
            19: 22, 20: 78, 21: 80, 22: 86, 23: 27, 24: 57, 25: 84}
ZoneDict = {1: 2, 2: 1, 3: 2, 4: 2, 5: 2, 6: 2, 7: 2, 8: 2, 9: 2, 10: 2, 11: 1,
            12: 2, 13: 2, 14: 2, 15: 1, 16: 2, 17: 2, 18: 1, 19: 2, 20: 1, 21:
            2, 22: 2, 23: 2, 24: 2, 25: 2}
SchoolDict = {1: "GO! atheneum Anderlecht",
              2: "KONINKLIJK ATHENEUM KOEKELBERG",
              3: "TSM-Bovenbouw"', 4: '"TSM-Bovenbouw",
              5: "PTS, Provinciale Scholen voor Tuinbouw en Techniek",
              6: "Onze-Lieve-Vrouw-Presentatie", 7: "Sint-Ludgardisschool",
              8: "Mater Salvatorisinstituut", 9: "Scheppersinstituut",
              10: "Sint-Gabriëlcollege", 11: "Scheppersinstituut",
              12: "Sint-Norbertusinstituut 2", 13: "Ursulinen Mechelen 1",
              14: "College Hagelstein 2", 15: "Scheppersinstituut",
              16: "TSM-Bovenbouw", 17: "TOEKOMSTONDERWIJS HOBOKEN",
              18: "Gemeentelijk Technisch Instituut",
              19: "Heilig Hart - Bovenbouw 1",
              20: "Gemeentelijke Technische en Beroepsschool",
              21: "GO! Busleyden Atheneum-campus Pitzemburg",
              22: "College Hagelstein 2",
              23: "Kardinaal van Roey-Instituut ASO",
              24: "Sint-Gummaruscollege", 25: "GO! atheneum Boom"}


idp.Type("Student", (1, 25))
idp.Type("Getal", (0, 10000000))
idp.Type("Postcode", (0, 100))
idp.Type("Zone", (0, 10))

idp.Predicate("Samen(Student,Student)")
idp.Predicate("VolSamen(Student, Student)")

idp.Function("Woont(Student): Postcode", WoonDict)
idp.Function("WoontZone(Student): Zone", ZoneDict)
idp.Function("School(Student): string", SchoolDict)

idp.Predicate("Wortel(Student)")
idp.Predicate("Blad(Student)")
idp.Function("Aant(Student): Getal")

idp.Constant("SamenSchool: Getal")
idp.Constant("Afstand: Getal")
idp.Constant("UitZone: Getal")
idp.Constant("Totaal: Getal")


idp.Define("Wortel(x) <- x < min{y[Student]: Samen(y,x):y}.", True)
idp.Define("Blad(x) <- ~Wortel(x).", True)

idp.Constraint("#{x[Student]: Wortel(x)} = 5", True)
idp.Constraint(" !x[Student]: Blad(x) <=> ?y[Student]: Wortel(y) & Samen(y,x)",
               True)
idp.Constraint("!x[Student]: Aant(x) = #{y[Student]: Samen(x,y) | Samen(y,x)}",
               True)
idp.Constraint("!x[Student]: Wortel(x) <=> 4 =< Aant(x) =< 6", True)
idp.Constraint("!x[Student]: Blad(x) <=> Aant(x) = 1", True)
idp.Constraint("!x[Student] y[Student]: Samen(x,y) => Wortel(x) & Blad(y)",
               True)
idp.Define("!x[Student] y[Student] z[Student]: VolSamen(y,z) <- Wortel(x) & y"
           " < z & Samen(x,y) & Samen(x,z).\n"
           "!x[Student] y[Student]: VolSamen(x,y) <- Wortel(x) & Samen(x,y).",
           True)
idp.Constraint("SamenSchool = #{x[Student] y[Student]: x < y & VolSamen(x,y)"
               "& School(x) ~= School(y)}", True)
idp.Constraint("Afstand = sum{x[Student] y[Student]: x < y & VolSamen(x,y) &"
               " WoontZone(x) = WoontZone(y): abs(Woont(x) - Woont(y))}", True)
idp.Constraint("UitZone = #{x[Student] y[Student]: x < y & VolSamen(x,y) &"
               " WoontZone(x) ~= WoontZone(y) }", True)

idp.Constraint("Totaal = Afstand  + UitZone * 100 + SamenSchool*100", True)


solutions = idp.model_expand()
solutions = idp.minimize("Totaal")
idp.check_sat()

print("{:d} solutions!".format(len(solutions)))
for index, sol in enumerate(solutions):
    if sol['satisfiable']:
        print("Sol {:d}:".format(index))
        for x in sol['Samen']:
            print('\t', x)
    else:
        continue


