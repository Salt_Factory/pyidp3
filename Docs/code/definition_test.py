#!/usr/bin/python3

"""
This testfile is a graph solver.
Given a set of edges, it finds out what nodes are connected.
To do this is uses a definition consisting of two rules.

"""
from pathlib import Path
from pyidp3.typedIDP import *


home = str(Path.home())
idp = IDP(home+"/idp/usr/local/bin/idp")

# Define Node, Edge and Connected
nodelist = ['A', 'B', 'C', 'D', 'E']
idp.Type("Node", nodelist)
idp.Predicate("Edge(Node, Node)", ["A,B", "A,C", "B,E", "C,D", "C,E", "D,E",
                                   "E,B"])
idp.Predicate("Connected(Node, Node)")

idp.Define("!x[Node] y[Node]: Connected(x,y) <- Edge(y,x). \n"
           "!x[Node] y[Node] z[Node]: Connected(x,y) "
           "<- Connected(x,z) & Connected(z,y)", True)

idp.check_sat()
idp.model_expand()



