#!/usr/bin/python3

"""
Testfile which attempts to assign students to a group, based on a previous
group and a preference.
"""
from pathlib import Path
from pyidp3.typedIDP import *


home = str(Path.home())
idp = IDP(home+"/idp/usr/local/bin/idp")
idp.nbmodels = 5

# All the inputvars
idp.Type("Student", (1, 152))
idp.Type("Number", (0, 10000000))
idp.Type("Group", (2, 11))
idp.Type("Priority", "Number", isa=True)
idp.Constant("MinSize: Number", 14)
idp.Constant("MaxSize: Number", 16)

together = [(142, 139, 2), (139, 143, 1), (139, 142, 1)]
not_together = [(142, 143, 2), (142, 141, 2), (142, 140, 2)]
in_group = []
group_dict = {1: 2,  2: 3,  3: 4,  4: 5,  5: 6,  6: 7,  7: 8,  8: 9,  9: 10,
              10: 11,  11: 3,  12: 11,  13: 8,  14: 6,  15: 4,  16: 2,  17: 10,
              18: 8,  19: 7,  20: 5,  21: 4,  22: 3,  23: 2,  24: 11,  25: 9,
              26: 8, 27: 6,  28: 6,  29: 5,  30: 4,  31: 3,  32: 2,  33: 10,
              34: 11,  35: 9,  36: 5,  37: 7,  38: 7,  39: 6,  40: 5,  41: 5,
              42: 4,  43: 3,  44: 3,  45: 2,  46: 10,  47: 10,  48: 11,  49: 8,
              50: 9,  51: 7,  52: 8, 53: 10,  54: 6,  55: 6,  56: 5,  57: 5,
              58: 5,  59: 4,  60: 4,  61: 3, 62: 3,  63: 2,  64: 2,  65: 10,
              66: 7,  67: 10,  68: 11,  69: 11,  70: 9,  71: 9,  72: 8,  73: 8,
              74: 7,  75: 7,  76: 6,  77: 6,  78: 6,  79: 9,  80: 5,  81: 5,
              82: 4,  83: 4,  84: 4,  85: 3,  86: 3,  87: 3,  88: 2,  89: 2,
              90: 2,  91: 2,  92: 10,  93: 11,  94: 10,  95: 8,  96: 11,
              97: 11,  98: 9,  99: 9,  100: 9,  101: 9,  102: 8,  103: 8,
              104: 8, 105: 7,  106: 7,  107: 7,  108: 6,  109: 6,  110: 6,
              111: 6,  112: 5, 113: 5,  114: 5,  115: 5,  116: 4,  117: 4,
              118: 4,  119: 4,  120: 4, 121: 3,  122: 3,  123: 3,  124: 3,
              125: 2,  126: 2,  127: 2,  128: 2, 129: 11,  130: 10,  131: 10,
              132: 10,  133: 10,  134: 10,  135: 11, 136: 11,  137: 11,
              138: 11,  139: 9,  140: 9,  141: 9,  142: 9,  143: 9,  144: 8,
              145: 8,  146: 8,  147: 7,  148: 7,  149: 7,  150: 7,  151:
              7,  152: 6}
idp.Function("InGroup(Student): Group", group_dict)
idp.Predicate("WantsTogether(Student, Student, Priority)", together)
idp.Predicate("NotWantsTogether(Student, Student, Priority)", not_together)
idp.Predicate("WantsInGroup(Student, Group, Priority)", in_group)


# All the inner workings + output
idp.Function("GroupSize(Group): Number")

idp.Constant("Total: Number")
idp.Constant("TotUnsatTogether: Number")
idp.Constant("TotUnsatNotTogether: Number")
idp.Constant("TotUnsatInGroup: Number")
idp.Constant("TotUnsatNewGroup: Number")

idp.Predicate("UnsatTogether(Student, Student)")
idp.Predicate("UnsatNotTogether(Student, Student)")
idp.Predicate("UnsatInGroup(Student, Group)")
idp.Predicate("UnsatNewGroup(Student)")

idp.Function("NewInGroup(Student): Group")


# All the necessary constraints
idp.Constraint("!g[Group]: GroupSize(g) = #{x[Student]: NewInGroup(x) = g}",
               True)
idp.Constraint("!g[Group]: MinSize =< GroupSize(g) =< MaxSize", True)
idp.Constraint("TotUnsatNewGroup = #{x[Student]: InGroup(x) ~= NewInGroup(x)}",
               True)
idp.Constraint("TotUnsatTogether = sum{x[Student] y[Student]:"
               " WantsTogether(x,y,p) & NewInGroup(x) ~= NewInGroup(y): p}",
               True)
idp.Constraint("TotUnsatNotTogether = sum{x[Student] y[Student]:"
               " NotWantsTogether(x,y,p) & NewInGroup(x) = NewInGroup(y): p}",
               True)
idp.Constraint("TotUnsatInGroup = sum{x[Student] g[Group]: WantsInGroup(x,g,p)"
               " & NewInGroup(x) ~= g: p}", True)
idp.Constraint("Total = TotUnsatTogether + TotUnsatNotTogether +"
               " TotUnsatInGroup + TotUnsatNewGroup", True)

idp.Define("!x[Student] y[Student]: UnsatTogether(x,y) <- WantsTogether(x,y,p)"
           " & NewInGroup(x) ~= NewInGroup(y).", True)
idp.Define("!x[Student] y[Student]: UnsatNotTogether(x,y) <- "
           " NotWantsTogether(x,y,p) & NewInGroup(x) = NewInGroup(y).",
           True)
idp.Define("!x[Student]: UnsatNewGroup(x) <- InGroup(x) ~= NewInGroup(x).",
           True)
idp.Define("!x[Student] g[Group]: UnsatInGroup(x,g) <- WantsInGroup(x,g,p) &"
           " NewInGroup(x) ~= g.", True)

sols = idp.minimize("Total")

newgroups = []
for sol in sols:
    newgroups.append(sol['NewInGroup'])
print(newgroups)
print("Verschil tussen sols:")
idp.compare(newgroups)
# idp.compare(sols)
