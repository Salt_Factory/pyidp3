#!/usr/bin/python3

"""
This is a very simple and small test.
If this test fails, there's something extremely wrong.
It defines 3 variables, that each are either True or False.
"""
from pathlib import Path
from pyidp3.typedIDP import IDP


home = str(Path.home())
idp = IDP(home+"/idp/usr/local/bin/idp")
idp.nbmodels = 2
idp.xsb = "true"

idp.Constant("P")
idp.Constant("Q")
idp.Constant("R")

idp.Constraint("(P => Q) <=> (P <= R).", True)
idp.Constraint("satisfiable().", True)

idp.check_sat()
idp.model_expand()


