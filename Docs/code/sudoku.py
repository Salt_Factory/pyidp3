#!/usr/bin/python3

"""
This testfile solves a sudoku.

"""
from pathlib import Path
from pyidp3.typedIDP import *


home = str(Path.home())
idp = IDP(home+"/idp/usr/local/bin/idp")
idp.nbmodels = 10

idp.Type("Row", (1, 9))
idp.Type("Column", (1, 9))
idp.Type("Number", (1, 9))

start_numbers = [(1, 1, 8), (1, 1, 8),
                 (2, 3, 3), (2, 4, 6),
                 (3, 2, 7), (3, 5, 9), (3, 7, 2),
                 (4, 2, 5), (4, 6, 7),
                 (5, 5, 4), (5, 6, 5), (5, 7, 7),
                 (6, 4, 1), (6, 8, 3),
                 (7, 3, 1), (7, 8, 6),
                 (7, 9, 8),
                 (8, 3, 8),
                 (8, 4, 5),
                 (8, 8, 1),
                 (9, 2, 9),
                 (9, 7, 4)]
idp.Predicate("Square(Row, Column, Row, Column)")
idp.Predicate("Start(Row,Column,Number)", start_numbers)
idp.Predicate("Group(Row, Column, Row, Column)")
idp.Function("Solution(Row, Column): Number")

idp.Constraint("!r[Row] c[Column] number: Start(r, c, number) =>"
               "Solution(r, c) = number", True)
idp.Define("!r1[Row] c1[Column] r2[Row] c2[Column]:"
           "Square(r1,c1,r2,c2) <- r1- (r1-1)%3 = "
           " r2 - (r2-1)%3 & c1-(c1-1)%3 = c2-(c2-1)%3",
           True)
idp.Define("!r1[Row] c1[Column] r2[Row] c2[Column]: Group(r1, c1, r2, c2) "
           "<- Square(r1, c1, r2, c2).\n"
           "!r1[Row] r2[Row] c[Column]: Group(r1, c, r2, c) <- true.\n"
           "!r[Row] c1[Row] c2[Column]: Group(r, c1, r, c2).", True)
idp.Constraint("!r1[Row] c1[Column] r2[Row] c2[Column]: Group(r1, c1, r2, c2)"
               "& (r1 ~= r2 | c1 ~= c2) => Solution(r1,c1) ~="
               "Solution(r2,c2)", True)
idp.check_sat()
sols = idp.model_expand()
for index, sol in enumerate(sols):
    if sol['satisfiable']:
        print("Sol{:d}: ".format(index), sol['Solution'])



