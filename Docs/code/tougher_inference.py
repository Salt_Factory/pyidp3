#!/usr/bin/python3

"""
This is a fairly simple test, it just adds more constants and constraints.
The generated IDP file will have 6 letters (each an int),
which have some constraints (A, E and I need to be even numbers,
while B, C and D need to be uneven).
A, B and C also can't be zero.
No two letters can be the same number.

The following puzzle needs to be solved:
    AI
    BA
+-----
   CDE

Or in other terms: AI + BA = CDE
"""
from pathlib import Path
from pyidp3.typedIDP import *


home = str(Path.home())
idp = IDP(home+"/idp/usr/local/bin/idp")

# Define the range of numbers
idp.Type("Decimal", (0, 9))

# Define the letters
letters = ['A', 'B', 'C', 'I', 'D', 'E']
even_letters = ['A', 'I', 'E']
for letter in letters:
    idp.Constant(letter+": Decimal")
    for letter2 in letters:
        if letter == letter2:
            continue
        idp.Constraint(letter + " ~= " + letter2, True)
    if letter in even_letters:
        idp.Constraint("Even(" + letter + ")", True)
    else:
        idp.Constraint("~Even(" + letter + ")", True)


# Define the Even numbers
idp.Predicate("Even(Decimal)", [0, 2, 4, 6, 8])

# No two letters can have the same value
idp.Constraint("A ~= B & A ~= C & A ~= I & A ~= D & A ~= E & B ~= C & B ~= I"
               "& B ~= D & B ~= E & C ~= I & C ~= D & C ~= E & I ~= D &"
               " I ~= E & D ~= E", True)
idp.Constraint("I + A + 10*A + 10*B = E + 10 * D + 100 * C", True)

idp.check_sat()
idp.model_expand()



