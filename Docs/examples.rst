.. _examples:

Examples using the Pyidp3 API
=============================

All of these examples were made to be used in my CI testing pipeline.
For each of the examples, I list up what elements are in it in case you're looking for an example of something specific.

simple_inference
----------------

As the title says, this is a simple inference test.
Contains:

* **nbmodels** and **xdb** options
* Defining *constants* without value or type
* *Constraint*
* Satchecking
* Model expanding

.. literalinclude:: code/simple_inference.py

harder_inference
----------------

This is a bit tougher than the previous example.
Contains:

* *Type* with a range
* *Constant* with type, no value
* *Constraint*
* *Predicate* 
* Satchecking
* Model expanding

.. literalinclude:: code/tougher_inference.py

definition_test
---------------

This is a graphsolver with a Define in it. 
Other than that, nothing special going on.
Contains:

* *Type*
* *Predicate*, with and without value
* *Define*
* Satchecking
* Modelexpansion

.. literalinclude:: code/definition_test.py

constructed_from
----------------

This file was mainly made to test the **constructed from** keyword.
It contains:

* *Type*, with **constructed from**
* *Predicate*, with value
* *Constraint*
* Satchecking
* Modelexpansion

.. literalinclude:: code/constructed_from.py

initial_group_assign
--------------------

This file is part of my master's thesis. 
It's a simplified version of my initial group assign, where the IDP-system tries to assign students to groups as best as it can.
It tries to minimize a term "Totaal".
This currently isn't in English, for which I'm sorry.

It contains:

* **mxtimeout** option
* *Types*, with lists as values
* *Predicate*
* *Function*
* *Constant*
* *Define*
* *Constraint*
* Satchecking
* Modelexpansion
* Minimization, using the *Totaal* term.

.. literalinclude:: code/small_initial.py

initial_group_assign_plus
-------------------------

This file is an expanded version of the previous, where schools are taken into account.

It contains:

* **mxtimeout** and **nbmodels** option
* *Types*, with lists as values
* *Predicate*
* *Function*
* *Constant*
* *Define*
* *Constraint*
* Satchecking
* Modelexpansion
* Minimization, using the *Totaal* term.

.. literalinclude:: code/big_initial.py

further_group_assign
--------------------

This is the other part of my master's thesis.
Here we try to reassign students based on their old groups, and a preference.

It contains:

* **nbmodels** option
* comparing solutions using the *compare* method
* *Type*, also one with the **isa** keyword
* *Constant*, with value
* *Predicate*
* *Function*
* *Constraint*
* *Define*
* minimization, using the *Total* term

.. literalinclude:: code/group_assign.py

sudoku
------
Sudokusolver.

Contains:

* **nbmodels** option
* *Type*
* *Constant*, with value
* *Predicate*
* *Function*
* *Constraint*
* *Define*
* Modelexpansion

.. literalinclude:: code/sudoku.py

masyu
-----

Contains:

* *Type*, **constructed_from**
* *Constant*, with value
* *Predicate*
* *Function*
* *Constraint*
* *Define*
* Satchecking
* Modelexpansion

.. literalinclude:: code/masyu.py
