.. _features:

Pyidp3 features
===============

Because Pyidp3 is a port of Pyidp, not all features were added by me. 



.. _existing_features:
Existing features from Pyidp
----------------------------
Here is the list of features that were already in Joost Vennekens' Pyidp (and were merely ported by me): 

* Parsing Python to IDP, and from IDP to Python.
* Converting Pythonic to IDP-form. *(This is no longer supported in Pyidp3. The code is there, so it might work but I didn't do any active development on it.)
* Support for *Type*, *Predicate*, *Function*, *Constant* and *Definitions*.
* Support for *vocabulary*, *theory* and *structure*.
* Basic model expansion.


.. _added_features:
Added features in Pyidp3
------------------------
Here is the list of features added in Pyidp3:

* Sphinx documentation (you're reading it!).
* Documentation throughout the code, to make it more readable.
* Support for adding the *Term* block, as a subclass of *Block*.
* Support for **constructed_from** keyword in a *Type*.
* Support for **isa** keyword in a *Type*.
* Model expansion is now done by calling *.model_expand()*.
* Implemented a way to minimize, by adding the *.minimize(term)* method.
* Implemented a way to SATcheck, by adding the *.sat_check()* method.
* Users can now also set IDP options (All options! Most of them haven't been tested though).
* The *model_expand* and *minimize* methods are now able to return multiple solutions, instead of only one.
* The IDP object now has a *compare* method to compare two enumerables and list the differences (currently only for dictionaries). 

Added QOL in Pyidp3:
--------------------
Along with features, some 'Quality-Of-Life' related functionalities were added to Pyidp3.
These are all features that aren't completely *necessary*, but are just nice to have (and improved my QOL as the maintainer of this module). 

* Changed the structure of the .py files to an actual module.

When porting I deleted the __init__.py file because I didn't know what it did. 
I imported all the submodules relatively, which was a massive pain.

* Added PEP8 conformity.

This is mostly codelines being longer than 79 characters, or forgetting whitespace.
These don't break functionality, but it makes everything harder to read!

* Automated testing!

Using GitLab's free CI and a custom-made Dockerfile, I was able to automate my testing. 
Thanks to this I was able to find a lot of bugs I had written before I pulled my code into the masterbranch.
Thanks GitLab!

* Written a .idp to .py converter.

During my testing I needed a lot of testfiles, and creating them all by hand took a while. 
Which is why I tried my best at creating a .idp to .py converter.
As of right now it mostly works, there's just some minor kinks I need to get out before I can call it actually done.

* Pyidp3 doesn't add a '.' to a constraint that already contains one at the end.

Before I constantly forgot that Pyidp3 automatically adds a '.' at the end of a constraint. 
Which means that if I manually added a '.', I would have two dots at the end of the line, and IDP wouldn't be able to interpret it.
Now it checks whether it needs to add a '.' before actually adding one.

Fixed bugs:
-----------

The original Pyidp version also had some bugs. However, I do suspect most of them were introduced by me when I first ported Pyidp to Pyidp3.
Still, here's a list of all the bugs that were fixed.

* Fixed a bug in the *Constraint* method, where it didn't use self.know but appended directly to itself. 
* *IDPIntTypes* are no longer generated number by number. 

.. code-block:: guess

    foo = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10} //old 
    foo = {0..10} //new

* 'string' wasn't being recognized as a possible type.
* String had to be supplied with extra quotation marks.
.. code-block:: guess

    Type(example, {1: '"first"', 2: '"second"'})

would be needed to translate to

.. code-block:: guess

    example = {1->"first"; 2->"second"}

Now this is no longer needed, and leaving the outer quotation marks is possible.

.. code-block:: guess

    Type(example, {1: "first", 2: "second"})

* Definitions are no longer formed incorrectly.
* Fixed the CI testing platform not having support for unicode utf-8 characters.
* Constants can now get a value:

.. code-block:: guess

    Constant("Apple", 5)

will now result in:

.. code-block:: guess

    Apple: 5

in the theory.

* 'nat' wasn't begin recognized as a possible type.
