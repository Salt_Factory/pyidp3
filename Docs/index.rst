.. Pyidp3 documentation master file, created by
   sphinx-quickstart on Thu Apr 11 13:38:21 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Pyidp3's documentation!
==================================

This documentation covers everything in the `Pyidp3 <https://gitlab.com/Salt_Factory/pyidp3>`_ module.

This is a Python3 port of `Joost Vennekens Pyidp <https://bitbucket.org/joostv/pyidp/src>`_.

The Pyidp3 module is an API between `Python3 <https://www.python.org/>`_ and `the IDP system <https://dtai.cs.kuleuven.be/software/idp>`_.
In short, IDP is a Knowledge Base System (KBS) using the FO(.) language.
FO(.) is standard First-Order logic, but expanded. See the IDP website for more.
A KBS is a system that stores all it's knowledge in a knowledge base, and then supports different inference methods to apply on the knowledge. It's programmed in a declarative manner. More on programming the IDP system and FO(.) can be found `here <https://dtai.cs.kuleuven.be/krr/files/TutorialIDP.pdf>`_.

Pyidp3 will try to bridge the gap between IDP (which is programmed *declaratively*) and Python (which is programmed *imperatively*).
It works in both directions: the user can supply data in Pythonic form to Pyidp3, which will then be converted to IDP form and given to the IDP system. 
When the IDP system is done infering, Pyidp3 will process it's output and translate this back into Pythonic form. 

.. image:: images/pyidp3.png

A list of all the features can be found at: :ref:`features`.

More information on the porting of Pyidp to Pyidp3 can be found at: :ref:`porting`.


This submodule is part of my master's thesis. Due to timeconstraints, this module is far from perfect.
I used it to build an application to assign students to groups, based on the IDP system.
More on that can be found `here <https://gitlab.com/Salt_Factory/idp_group_assign_application>`_.


.. toctree::
   :maxdepth: 2
   :caption: Contents: 

   features
   tutorial
   examples
   porting
   api



