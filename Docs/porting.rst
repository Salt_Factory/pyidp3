.. _porting:

Porting of Pyidp to Pyidp3
==========================

* Firstly Python's own `2to3 <https://docs.python.org/3.7/library/2to3.html>`_ was used for the initial port. This is a tool to *automagically* convert from Python2 to Python3. This produces a log of the changes made, which can be found at the bottom of this page.
* The way *popen* works was changed to only accept byte objects (and it doesn't convert them automatically), so I had make sure all input was encoded first, and all output was decoded afterwards. This can be done easily by using the *encode()* and *decode()* methods.

That's it. Using these two simple tricks, I was able to convert Pyidp to work on Python3 (*Doctors hate him!*).
After porting, features were added, QOL was improved and bugs were squashed. More on that can be found at :ref:`features`.

.. literalinclude:: code/2to3.txt
