from setuptools import setup

setup(
    name='pyidp3',
    version='0.0.9',
    description='A Python3 to IDP API',
    author='Simon Vandevelde',
    author_email="s.vandevelde@kuleuven.be",
    url="https://gitlab.com/Salt_Factory/pyidp3",
    packages=['pyidp3'],
)
