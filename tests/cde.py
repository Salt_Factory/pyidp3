from pathlib import Path
from pyidp3.typedIDP import *

home = str(Path.home())
idp = IDP(home+"/idp/usr/local/bin/idp")

idp.Type("Decimaal", (0, 9))
idp.Constant("A: Decimaal")
idp.Constant("B: Decimaal")
idp.Constant("C: Decimaal")
idp.Constant("I: Decimaal")
idp.Constant("D: Decimaal")
idp.Constant("E: Decimaal")
idp.Predicate("Even( Decimaal)", [0, 2, 4, 6, 8])

idp.model_expand()
