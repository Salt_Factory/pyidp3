#!/usr/bin/python3

"""
This is by far the testfile with the most in it (yet it's not the hardest!).
It solves a masyu puzzle.

"""
from pathlib import Path
from pyidp3.typedIDP import *


home = str(Path.home())
idp = IDP(home+"/idp/usr/local/bin/idp")

idp.Type("Row", (0, 4))
idp.Type("Column", (0, 4))
idp.Type("Pearl", ["Hollow", "Filled"], constructed_from=True)
idp.Type("Wire", ["NS", "EW", "ES", "WS", "NE", "NW", "Empty"],
         constructed_from=True)

pearl_position = [(0, 2, "Filled"),
                  (1, 4, "Filled"),
                  (2, 2, "Filled"),
                  (3, 0, "Filled"), (3, 1, "Filled"),
                  (4, 4, "Hollow")]
idp.Predicate("PearlPosition(Row, Column, Pearl)", pearl_position)
idp.Predicate("WireStraight(Wire)", ["NS", "EW"])
idp.Predicate("WireCurve(Wire)", ["ES", "WS", "NE", "NW"])
idp.Predicate("WireNorth(Wire)", ["NS", "NE", "NW"])
idp.Predicate("WireEast(Wire)", ["ES", "NE", "EW"])
idp.Predicate("WireSouth(Wire)", ["NS", "WS", "ES"])
idp.Predicate("WireWest(Wire)", ["EW", "WS", "NW"])
idp.Predicate("Link(Row, Column, Row, Column)")
idp.Predicate("Connected(Row, Column, Row, Column)")
idp.Function("Solution(Row, Column): Wire")

idp.Constraint("!r c: WireNorth(Solution(r,c)) => WireSouth(Solution(r-1,c))",
               True)
idp.Constraint("!r c: WireSouth(Solution(r,c)) => WireNorth(Solution(r+1,c))",
               True)
idp.Constraint("!r c: WireEast(Solution(r,c)) => WireWest(Solution(r,c+1))",
               True)
idp.Constraint("!r c: WireWest(Solution(r,c)) => WireEast(Solution(r,c-1))",
               True)
idp.Constraint("!r c parel: PearlPosition(r,c,parel) & parel = Hollow => "
               "WireCurve(Solution(r,c))", True)
idp.Constraint("!r c parel: PearlPosition(r,c,parel) & parel = Filled => "
               "WireStraight(Solution(r,c))", True)
idp.Constraint("!r c parel: PearlPosition(r,c,parel) & parel = Hollow & "
               "WireNorth(Solution(r,c)) => WireStraight(Solution(r-1,c))",
               True)
idp.Constraint("!r c parel: PearlPosition(r,c,parel) & parel = Hollow & "
               "WireSouth(Solution(r,c)) => WireStraight(Solution(r+1,c))",
               True)
idp.Constraint("!r c parel: PearlPosition(r,c,parel) & parel = Hollow & "
               "WireEast(Solution(r,c)) => WireStraight(Solution(r,c+1))",
               True)
idp.Constraint("!r c parel: PearlPosition(r,c,parel) & parel = Hollow & "
               "WireWest(Solution(r,c)) => WireStraight(Solution(r,c-1))",
               True)
idp.Constraint("!r c parel: PearlPosition(r,c,parel) & parel = Filled & "
               "WireNorth(Solution(r,c)) => (WireCurve(Solution(r-1,c)) | "
               "WireCurve(Solution(r+1,c)))", True)
idp.Constraint("!r c parel: PearlPosition(r,c,parel) & parel = Filled & "
               "WireEast(Solution(r,c)) => (WireCurve(Solution(r,c-1)) | "
               "WireCurve(Solution(r,c+1)))", True)
idp.Constraint("!r1 c1 r2 c2: (Solution(r1,c1) ~= Empty & Solution(r2,c2) ~= "
               "Empty) => Connected(r1,c1,r2,c2)", True)
idp.Define("!r c: Link(r,c,r-1,c) <- WireNorth(Solution(r,c)) & "
           "WireSouth(Solution(r-1,c)). "
           "!r c: Link(r,c,r+1,c) <- WireSouth(Solution(r,c)) & "
           "WireNorth(Solution(r+1,c)). "
           "!r c: Link(r,c,r,c+1) <- WireEast(Solution(r,c)) & "
           "WireWest(Solution(r,c+1)). "
           "!r c: Link(r,c,r,c-1) <- WireWest(Solution(r,c)) &"
           "WireEast(Solution(r,c-1)).", True)

idp.Define("!r1 c1 r2 c2: Connected(r1,c1,r2,c2) <- Link(r1,c1,r2,c2). "
           "!r1 c1 r2 c2: Connected(r1,c1,r2,c2) <- ?r3 k3: "
           "Connected(r3,k3,r2,c2) & Connected(r1,c1,r3,k3) ", True)
idp.check_sat()
idp.model_expand()



