#!/bin/bash

files="simple_inference.py tougher_inference.py definition_test.py constructed_from.py small_initial.py big_initial.py group_assign.py sudoku.py masyu.py"

for file in $files; do
    ding=$(python3 ./$file 2>&1)
    dong=$(echo $ding | grep -Eo 'Error' | wc -l )
    if  [[ ($dong -ne 1) ]]; then
        echo "$file: $dong errors!"
        exit -1
    fi
    echo "$file: all good!"

done
exit 0
