"""Test for the <ct> flag using Sudoku as example."""
from pathlib import Path
from pyidp3.typedIDP import IDP

home = str(Path.home())
idp = IDP(home+"/idp/usr/local/bin/idp")
idp.xsb = "true"

idp.Type(name='Row', enumeration=(0, 8), isa='nat')
idp.Type(name='Col', enumeration=(0, 8), isa='nat')
idp.Type(name='Value', enumeration=(1, 9), isa='nat')
idp.Predicate('sameRow(Row,Col,Row,Col)')
idp.Predicate('sameCol(Row,Col,Row,Col)')
idp.Predicate('sameBox(Row,Col,Row,Col)')
idp.Define(
    '!r1,c1,r2,c2 : sameRow(r1,c1,r2,c2) <- r1 = r2 & c1 ~= c2.',
    True
)
idp.Define(
    '!r1, c1, r2, c2: sameCol(r1, c1, r2, c2) <- r1 ~=r2 & c1=c2.',
    True
)
idp.Define(
    '!r1,c1,r2,c2 : sameBox(r1,c1,r2,c2) <-' +
    '( r1 - r1 % 3 = r2 - r2 % 3 ) &' +
    '( c1 - c1 % 3 = c2 - c2 % 3 ) &' +
    '( r1 ~= r2 | c1 ~= c2 ).',
    True
)
idp.Constraint('!r,c : ?v : gridValue(r,c,v)', True)
idp.Constraint(
    '!r1,c1,v1,r2,c2,v2 : sameRow(r1,c1,r2,c2) &' +
    'gridValue(r1,c1,v1) &' +
    'gridValue(r2,c2,v2) => v1 ~= v2.',
    True
)
idp.Constraint(
    '!r1,c1,v1,r2,c2,v2 : sameCol(r1,c1,r2,c2) &' +
    'gridValue(r1,c1,v1) &' +
    'gridValue(r2,c2,v2) => v1 ~= v2.',
    True
)
idp.Constraint(
    '!r1,c1,v1,r2,c2,v2 : sameBox(r1,c1,r2,c2) &' +
    'gridValue(r1,c1,v1) &' +
    'gridValue(r2,c2,v2) => v1 ~= v2.',
    True
)

given = [
    (0, 3, 4), (0, 8, 9), (1, 1, 3), (1, 2, 8), (1, 5, 6), (1, 6, 7),
    (2, 0, 9), (2, 1, 5), (2, 6, 6), (2, 8, 2), (3, 0, 2), (3, 1, 1),
    (3, 2, 3), (3, 4, 9), (3, 5, 5), (3, 7, 7), (3, 8, 6), (4, 3, 2),
    (4, 7, 5), (4, 8, 3), (5, 0, 7), (5, 1, 9), (5, 3, 3), (5, 4, 6),
    (5, 5, 4), (5, 7, 8), (5, 8, 1), (6, 4, 8), (6, 5, 9), (6, 8, 7),
    (7, 1, 2), (7, 2, 9), (7, 3, 7), (7, 4, 4), (8, 0, 8), (8, 6, 3)
]
idp.Predicate('gridValue(Row,Col,Value)', enumeration=given, ct=True)
sat = idp.check_sat()
