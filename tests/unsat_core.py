"""Test for the printunsatcore inference task."""

from pathlib import Path
from pyidp3.typedIDP import IDP

home = str(Path.home())
idp = IDP(home+"/idp/usr/local/bin/idp")
idp.xsb = "true"

idp.Type(name='Number', enumeration=(1,10), isa='int')
idp.Predicate('IsSquare(Number,Number)')
idp.Define('!x,y : IsSquare(x,y) <- y = x * x.', True)

idp.Constraint('~IsSquare(2,4).', True)  # this will cause unsat
unsat_core = idp.printunsatcore()
print(unsat_core)
